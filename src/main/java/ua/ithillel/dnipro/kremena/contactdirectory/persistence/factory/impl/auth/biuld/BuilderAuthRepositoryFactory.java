package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.auth.biuld;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.database.template.JdbcTemplate;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.AuthRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.auth.ApiAuthRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.auth.DatabaseAuthRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.auth.InMemoryAuthRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.util.encoder.PasswordEncoder;

import java.net.http.HttpClient;
import java.util.Map;
import java.util.Properties;

@RequiredArgsConstructor
public class BuilderAuthRepositoryFactory {

    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;
    private final PasswordEncoder passwordEncoder;
    private final JdbcTemplate jdbcTemplate;

    public AuthRepositoryFactory getAuthRepositoryFactory(Properties properties, String applicationMode) {
        String mode = properties.getProperty(applicationMode);
        Map<String, AuthRepositoryFactory> factories =
                Map.of(
                        "IN_MEMORY", new InMemoryAuthRepositoryFactory(),
                        "BYTE_FILE", new InMemoryAuthRepositoryFactory(),
                        "TEXT_FILE", new InMemoryAuthRepositoryFactory(),
                        "JSON_FILE", new InMemoryAuthRepositoryFactory(),
                        "API", new ApiAuthRepositoryFactory(httpClient, properties, objectMapper),
                        "DB", new DatabaseAuthRepositoryFactory(jdbcTemplate, passwordEncoder)
                );
        if (!factories.containsKey(mode)) {
            throw new RuntimeException("Invalid mode configuration");
        }
        return factories.get(mode);
    }
}