package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.factory;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.service.contact.ContactService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.MenuItem;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.item.*;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.AuthView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ContactView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ErrorView;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
public class BuilderMenuItems {

    private final ContactView contactView;
    private final ErrorView errorView;
    private final AuthView authView;
    private final AuthService authService;
    private final ContactService contactService;

    public List<MenuItem> getMenuItems(boolean auth) {

        Map<Boolean, List<MenuItem>> factories = Map.of(
                false, List.of(
                        new LoginMenuItem(authService, authView, errorView),
                        new RegistrationMenuItem(authService, authView, errorView),
                        new ExitMenuItem(authService)),
                true, List.of(
                        new AddContactMenuItem(contactService, contactView, errorView),
                        new ShowAllMenuItem(contactService, contactView),
                        new ShowOnlyPhonesMenuItem(contactService, contactView),
                        new ShowOnlyEmailMenuItem(contactService, contactView),
                        new SearchByNameMenuItem(contactService, contactView),
                        new SearchByStartContactMenuItem(contactService, contactView),
                        new DeleteContactByValue(contactService, contactView, errorView),
                        new LogoutMenuItem(authService, errorView),
                        new ExitMenuItem(authService)
                )
        );
        return factories.get(auth);
    }
}