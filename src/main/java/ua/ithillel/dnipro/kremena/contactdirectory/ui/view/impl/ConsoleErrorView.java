package ua.ithillel.dnipro.kremena.contactdirectory.ui.view.impl;

import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ErrorView;
import ua.ithillel.dnipro.kremena.contactdirectory.util.console.PrintConsole;

public class ConsoleErrorView implements ErrorView {

    @Override
    public void showError(String errorMessage) {
        PrintConsole.printDelimiter();
        PrintConsole.printMessage("!!!   Error   !!!");
        PrintConsole.printMessage(errorMessage);
        PrintConsole.printDelimiter();
    }
}