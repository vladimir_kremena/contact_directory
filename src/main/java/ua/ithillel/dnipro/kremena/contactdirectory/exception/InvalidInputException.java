package ua.ithillel.dnipro.kremena.contactdirectory.exception;

public class InvalidInputException extends RuntimeException {

    public InvalidInputException(String message) {
        super(message);
    }
}