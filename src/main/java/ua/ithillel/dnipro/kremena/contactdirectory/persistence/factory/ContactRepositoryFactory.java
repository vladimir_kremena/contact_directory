package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory;

import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;

public interface ContactRepositoryFactory {

    ContactRepository createContactRepository();

}