package ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file;

import ua.ithillel.dnipro.kremena.contactdirectory.util.file.FileUtil;

import java.io.File;

public abstract class AbstractFileContact implements FileContact {

    @Override
    public void isExists(String fileName) {
        File byteFile = new File(fileName);
        if (!byteFile.exists()) {
            FileUtil.createDirectoryAndFile(byteFile);
        }
    }
}