package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.item;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.service.contact.ContactService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.MenuItem;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ContactView;

@RequiredArgsConstructor
public class SearchByStartContactMenuItem implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Search by start of contact";
    }

    @Override
    public void run() {
        contactView.showContacts(contactService.findByValueStart(contactView.readStartContact()));
        contactView.pause();
    }
}