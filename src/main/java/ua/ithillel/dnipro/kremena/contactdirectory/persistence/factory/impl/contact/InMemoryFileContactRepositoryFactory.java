package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.contact;

import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.memory.InMemoryContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.ContactRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.cache.CachedContactRepository;

import java.util.ArrayList;
import java.util.List;

public class InMemoryFileContactRepositoryFactory implements ContactRepositoryFactory {

    @Override
    public ContactRepository createContactRepository() {
        return new CachedContactRepository(
                new InMemoryContactRepository(initialContacts())
        );
    }

    private List<Contact> initialContacts() {
        return new ArrayList<>(
                List.of(new Contact()
                                .setId("e507d7e1-b787-4ebe-9977-2a65cb1f5c86")
                                .setFirstName("Ирина")
                                .setLastName("Боброва")
                                .setContactType(ContactType.PHONE)
                                .setValue("+380660599551"),
                        new Contact()
                                .setId("b691dba7-20a6-405b-b955-8d2cfc2550fb")
                                .setFirstName("Ирина")
                                .setLastName("Боброва")
                                .setContactType(ContactType.EMAIL)
                                .setValue("bobrova.ia@it.ua"),
                        new Contact()
                                .setId("d34c5fdb-cb63-4304-9af6-5e3e7e5eebc5")
                                .setFirstName("Анна")
                                .setLastName("Иванова")
                                .setContactType(ContactType.PHONE)
                                .setValue("+380938117679"),
                        new Contact()
                                .setId("5d84338a-0cb4-47df-aa4b-adee67296200")
                                .setFirstName("Анна")
                                .setLastName("Иванова")
                                .setContactType(ContactType.EMAIL)
                                .setValue("ivanova.ak@it.ua"),
                        new Contact()
                                .setId("4c167cf8-8f16-4e4f-b565-ecdcb2104432")
                                .setFirstName("Александр")
                                .setLastName("Мединец")
                                .setContactType(ContactType.PHONE)
                                .setValue("+380943484625"),
                        new Contact()
                                .setId("b64eee04-b68a-4500-86f3-45364f225a73")
                                .setFirstName("Александр")
                                .setLastName("Мединец")
                                .setContactType(ContactType.EMAIL)
                                .setValue("medinets.aa@it.ua"),
                        new Contact()
                                .setId("52974406-ba5a-4cc7-9537-1be2ae49ab11")
                                .setFirstName("Никита")
                                .setLastName("Плясов")
                                .setContactType(ContactType.PHONE)
                                .setValue("+380979497286"),
                        new Contact()
                                .setId("3f18af98-9a20-4280-8fe1-dc55766fdf13")
                                .setFirstName("Никита")
                                .setLastName("Плясов")
                                .setContactType(ContactType.EMAIL)
                                .setValue("plyasov.nn@it.ua"),
                        new Contact()
                                .setId("df3991d9-9acf-46b3-bcaf-0d8431fceabb")
                                .setFirstName("Александр")
                                .setLastName("Ткаченко")
                                .setContactType(ContactType.PHONE)
                                .setValue("+380961322034"),
                        new Contact()
                                .setId("8b3b2cba-a291-4820-a069-f498ab809f23")
                                .setFirstName("Александр")
                                .setLastName("Ткаченко")
                                .setContactType(ContactType.EMAIL)
                                .setValue("tkachenco.aa@it.ua"),
                        new Contact()
                                .setId("65d59f22-7418-420d-b999-e2acce5a3d99")
                                .setFirstName("Катерина")
                                .setLastName("Шевченко")
                                .setContactType(ContactType.PHONE)
                                .setValue("+380966140395"),
                        new Contact()
                                .setId("7cbd6f28-7c95-4bfe-b6a1-d55ecfeb9ddf")
                                .setFirstName("Катерина")
                                .setLastName("Шевченко")
                                .setContactType(ContactType.EMAIL)
                                .setValue("shevchenko.km@it.ua"),
                        new Contact()
                                .setId("28df4137-27e4-42e7-bffd-da6ebcda6e50")
                                .setFirstName("Денис")
                                .setLastName("Шувалов")
                                .setContactType(ContactType.PHONE)
                                .setValue("+380942923723"),
                        new Contact()
                                .setId("f2e8d44b-4bba-4065-8d0d-8bb921636f1b")
                                .setFirstName("Денис")
                                .setLastName("Шувалов")
                                .setContactType(ContactType.EMAIL)
                                .setValue("shuvalov.dv@it.ua")
                )
        );
    }
}