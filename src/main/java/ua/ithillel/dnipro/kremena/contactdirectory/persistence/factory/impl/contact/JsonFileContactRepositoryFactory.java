package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.contact;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file.FileContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file.impl.JsonFileContact;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.ContactRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.cache.CachedContactRepository;

import java.util.Properties;

@RequiredArgsConstructor
public class JsonFileContactRepositoryFactory implements ContactRepositoryFactory {

    private final Properties properties;

    @Override
    public ContactRepository createContactRepository() {
        return new CachedContactRepository(
                new FileContactRepository(
                        new JsonFileContact(
                                properties.getProperty("json.file.name"),
                                createObjectMapper()
                        )
                )
        );
    }

    private ObjectMapper createObjectMapper() {
        return new ObjectMapper();
    }
}