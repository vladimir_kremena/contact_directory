package ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file;

import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;

import java.util.List;

public interface FileContact {

    void isExists(String fileName);

    List<Contact> readAll();

    void saveAll(List<Contact> contactList);

}