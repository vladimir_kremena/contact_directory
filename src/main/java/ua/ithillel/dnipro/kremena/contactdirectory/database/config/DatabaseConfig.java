package ua.ithillel.dnipro.kremena.contactdirectory.database.config;

import com.zaxxer.hikari.HikariConfig;
import lombok.RequiredArgsConstructor;

import java.util.Properties;

@RequiredArgsConstructor
public class DatabaseConfig {

    private final Properties properties;

    public HikariConfig getHikariConfig() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(properties.getProperty("database.base.uri"));
        hikariConfig.setUsername(properties.getProperty("database.user"));
        hikariConfig.setPassword(properties.getProperty("database.password"));
        try {
            hikariConfig.setMinimumIdle(Integer.parseInt(properties.getProperty("minimum.idle")));
            hikariConfig.setMaximumPoolSize(Integer.parseInt(properties.getProperty("maximum.pool.size")));
        } catch (NumberFormatException nFE) {
            System.out.println(nFE.getMessage());
        }
        return hikariConfig;
    }
}