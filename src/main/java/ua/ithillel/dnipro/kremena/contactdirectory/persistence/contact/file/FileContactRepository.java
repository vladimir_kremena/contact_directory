package ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class FileContactRepository implements ContactRepository {

    private final FileContact fileContact;

    @Override
    public void save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        List<Contact> contactList = findAll();
        contactList.add(contact);
        fileContact.saveAll(
                contactList.stream()
                        .sorted(Contact.byLastNameComparator())
                        .collect(Collectors.toList())
        );
    }

    @Override
    public void deleteByValue(String value) {
        fileContact.saveAll(
                findAll().stream()
                        .filter(contact -> !contact.getValue().equals(value))
                        .collect(Collectors.toList()));
    }

    @Override
    public List<Contact> findAll() {
        return fileContact.readAll();
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        return  findAll().stream()
                .filter(contact -> contact.getContactType() == type)
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return  findAll().stream()
                .filter(contact -> contact.getValue().equals(value))
                .findFirst();
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return  findAll().stream()
                .filter(contact -> contact.getValue().startsWith(valueStart))
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return  findAll().stream()
                .filter(contact -> contact.getFullName().toLowerCase().contains(namePart.toLowerCase()))
                .collect(Collectors.toList());
    }
}