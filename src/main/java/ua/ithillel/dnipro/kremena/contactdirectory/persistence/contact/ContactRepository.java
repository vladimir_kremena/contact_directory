package ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact;

import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;

import java.util.List;
import java.util.Optional;

public interface ContactRepository {

    void save(Contact contact);

    void deleteByValue(String value);

    List<Contact> findAll();

    List<Contact> findByType(ContactType type);

    Optional<Contact> findByValue(String value);

    List<Contact> findByValueStart(String valueStart);

    List<Contact> findByName(String namePart);

}