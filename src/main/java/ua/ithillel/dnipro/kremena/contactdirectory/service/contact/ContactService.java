package ua.ithillel.dnipro.kremena.contactdirectory.service.contact;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.exception.ContactNotFoundException;
import ua.ithillel.dnipro.kremena.contactdirectory.exception.DuplicateContactException;
import ua.ithillel.dnipro.kremena.contactdirectory.exception.ValidateException;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.util.validator.Validator;
import ua.ithillel.dnipro.kremena.contactdirectory.util.validator.contact.PhoneValidator;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ContactService {

    private final ContactRepository contactRepository;
    private final Validator<Contact> contactValidator;

    public void save(Contact contact) {
        if (!contactValidator.isValid(contact)) {
            throw new ValidateException("Contact is not valid");
        }
        if (contact.getContactType() == ContactType.PHONE) {
            contact.setValue("+380" + new PhoneValidator().getBody(contact));
        }
        if (contactRepository.findByValue(contact.getValue()).isPresent()) {
            throw new DuplicateContactException("Contact already exists !!!");
        }
        contactRepository.save(contact);
    }

    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    public List<Contact> findPhones() {
        return contactRepository.findByType(ContactType.PHONE);
    }

    public List<Contact> findEmails() {
        return contactRepository.findByType(ContactType.EMAIL);
    }

    public List<Contact> findByName(String namePart) {
        if (namePart.equals("")) return new ArrayList<>();
        return contactRepository.findByName(namePart);
    }

    public List<Contact> findByValueStart(String valueStart) {
        if (valueStart.equals("")) return new ArrayList<>();
        return contactRepository.findByValueStart(valueStart);
    }

    public void deleteByValue(String value) {
        contactRepository.findByValue(value)
                .orElseThrow(() -> new ContactNotFoundException("Contact not found !!!"));
        contactRepository.deleteByValue(value);
    }
}