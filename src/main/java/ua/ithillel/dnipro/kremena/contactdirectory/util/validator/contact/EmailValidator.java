package ua.ithillel.dnipro.kremena.contactdirectory.util.validator.contact;

import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;
import ua.ithillel.dnipro.kremena.contactdirectory.util.validator.Validator;

import java.util.regex.Pattern;

public class EmailValidator implements Validator<Contact> {

    private final static Pattern EMAIL_PATTERN = Pattern.compile("([0-9a-z.]+)@([0-9a-z]+\\.){1,2}[a-z]+");

    @Override
    public boolean isValid(Contact contact) {
        if (contact.getContactType() != ContactType.EMAIL) return true;
        return EMAIL_PATTERN.matcher(contact.getValue()).matches();
    }
}