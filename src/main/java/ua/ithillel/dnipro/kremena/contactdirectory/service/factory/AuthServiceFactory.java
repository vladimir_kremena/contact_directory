package ua.ithillel.dnipro.kremena.contactdirectory.service.factory;

import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;

public interface AuthServiceFactory {

    AuthService createAuthService();

}