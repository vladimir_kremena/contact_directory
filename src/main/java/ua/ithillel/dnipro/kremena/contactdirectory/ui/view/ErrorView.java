package ua.ithillel.dnipro.kremena.contactdirectory.ui.view;

public interface ErrorView {

    void showError(String errorMessage);

}