package ua.ithillel.dnipro.kremena.contactdirectory.service.factory.impl.auth;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.impl.DatabaseAuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.service.factory.AuthServiceFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.util.encoder.PasswordEncoder;

@RequiredArgsConstructor
public class DatabaseAuthServiceFactory implements AuthServiceFactory {

    private final AuthRepository authRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public AuthService createAuthService() {
        return new DatabaseAuthService(authRepository, passwordEncoder);
    }
}