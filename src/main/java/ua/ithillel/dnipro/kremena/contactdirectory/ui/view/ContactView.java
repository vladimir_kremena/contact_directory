package ua.ithillel.dnipro.kremena.contactdirectory.ui.view;

import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;

import java.util.List;

public interface ContactView {

    Contact readContact();

    void showContacts(List<Contact> contactList);

    void pause();

    String readNamePart();

    String readStartContact();

    String readValue();

}