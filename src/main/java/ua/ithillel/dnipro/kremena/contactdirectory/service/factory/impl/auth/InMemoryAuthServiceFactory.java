package ua.ithillel.dnipro.kremena.contactdirectory.service.factory.impl.auth;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.impl.InMemoryAuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.service.factory.AuthServiceFactory;

@RequiredArgsConstructor
public class InMemoryAuthServiceFactory implements AuthServiceFactory {

    private final AuthRepository authRepository;

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService(authRepository);
    }
}