package ua.ithillel.dnipro.kremena.contactdirectory.exception;

public class ContactNotFoundException extends RuntimeException {

    public ContactNotFoundException(String message) {
        super(message);
    }
}