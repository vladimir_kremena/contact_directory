package ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.impl;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.database.mapper.BeanPropertyRowMapper;
import ua.ithillel.dnipro.kremena.contactdirectory.database.template.JdbcTemplate;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Token;
import ua.ithillel.dnipro.kremena.contactdirectory.model.User;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.util.encoder.PasswordEncoder;

import java.util.Objects;
import java.util.UUID;

@RequiredArgsConstructor
public class DatabaseAuthRepository implements AuthRepository {

    private final JdbcTemplate jdbcTemplate;
    private final PasswordEncoder passwordEncoder;

    @Getter
    private Token token;

    @Getter
    private User authUser;

    @Override
    public void login(User user) {
        authUser = jdbcTemplate.query(
                "select id, login, password, date_born, date_registration from users where login = ?",
                new Object[] {
                        user.getLogin()
                },
                new BeanPropertyRowMapper<>(User.class)
            ).stream().findFirst().get();
        if (passwordEncoder.verify(user.getPassword(), authUser.getPassword())) {
            token = jdbcTemplate.query(
                    "select value, create_time, shutdown_time, valid from tokens where user_id = ? and valid = true",
                    new Object[] {
                            authUser.getId()
                    },
                    new BeanPropertyRowMapper<>(Token.class)
            ).stream()
                    .findFirst()
                    .orElse(
                            new Token()
                                    .setValue(UUID.randomUUID().toString())
                                    .setValid(false)
                    );
        }
        if(!token.isValid()) {
            token.setValid(true);
            jdbcTemplate.query(
                    "insert into tokens (value, valid, user_id) values (?, ?, ?)",
                    new Object[] {
                            token.getValue(),
                            token.isValid(),
                            authUser.getId()
                    });
        }
    }

    @Override
    public void logout() {
        jdbcTemplate.query(
                "update tokens set valid = false, shutdown_time = now() where value = ?",
                new Object[] {
                        token.getValue()
                });
        token = jdbcTemplate.query(
                "select value, create_time, shutdown_time, valid from tokens where value = ?",
                new Object[] {
                        token.getValue()
                },
                new BeanPropertyRowMapper<>(Token.class)
        ).stream().findFirst().get();
    }

    @Override
    public void registration(User user) {
        jdbcTemplate.query("insert into users (login, password, date_born) values (?, ?, ?)",
                new Object[] {
                        user.getLogin(),
                        user.getPassword(),
                        user.getDateBorn()
                });
    }

    @Override
    public boolean isAuth() {
        if (Objects.isNull(token) || Objects.isNull(token.isValid())) return false;
        else return token.isValid();
    }

}
