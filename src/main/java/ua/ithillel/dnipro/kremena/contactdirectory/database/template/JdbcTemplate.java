package ua.ithillel.dnipro.kremena.contactdirectory.database.template;

import lombok.RequiredArgsConstructor;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@RequiredArgsConstructor
public class JdbcTemplate {

    private final DataSource dataSource;

    public void query(String query, Object[] params) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            if (params.length != 0) {
                for (int i = 0; i < params.length; i++) {
                    preparedStatement.setObject(i + 1, params[i]);
                }
            }
            preparedStatement.execute();
        } catch (SQLException sqlE) {
            throw new RuntimeException(sqlE);
        }
    }

    public <T> List<T> query(String query, Object[] params, Function<ResultSet, T> converter) {
        try(Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            for(int i = 0; i < params.length; i++) {
                preparedStatement.setObject(i + 1, params[i]);
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            List<T> list = new ArrayList<>();
            while (resultSet.next()) {
                list.add(converter.apply(resultSet));
            }
            return list;
        } catch (SQLException sqlE) {
            sqlE.printStackTrace();
        }
        return null;
    }
}