package ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth;

import ua.ithillel.dnipro.kremena.contactdirectory.model.Token;
import ua.ithillel.dnipro.kremena.contactdirectory.model.User;

public interface AuthRepository {

    void login(User user);

    void logout();

    void registration(User user);

    boolean isAuth();

    Token getToken();

    User getAuthUser();

}