package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.item;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.exception.DuplicateContactException;
import ua.ithillel.dnipro.kremena.contactdirectory.exception.ValidateException;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.service.contact.ContactService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.MenuItem;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ContactView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ErrorView;

@RequiredArgsConstructor
public class AddContactMenuItem implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Add contact";
    }

    @Override
    public void run() {
        try {
            Contact contact = contactView.readContact();
            contactService.save(contact);
        } catch (DuplicateContactException dCE) {
            errorView.showError(dCE.getMessage());
        } catch (ValidateException vE) {
            errorView.showError(vE.getMessage());
        }
    }
}