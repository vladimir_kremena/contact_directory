package ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.dto.AddContactResponse;
import ua.ithillel.dnipro.kremena.contactdirectory.dto.ContactsResponse;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.util.console.PrintConsole;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ApiContactRepository implements ContactRepository {

    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;
    private final AuthService authService;
    private final Properties properties;

    @Override
    public void save(Contact contact) {
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(contact)))
                    .headers(   "Authorization", "Bearer " + authService.getToken().getValue(),
                                "Accept", "application/json",
                                "Content-Type", "application/json")
                    .uri(URI.create(properties.getProperty("mag.api.base.uri") + "contacts/add"))
                    .build();
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                AddContactResponse addContactResponse =
                        objectMapper.readValue(httpResponse.body(), AddContactResponse.class);
                if (addContactResponse.isSuccess()) {
                    PrintConsole.printDelimiter();
                    PrintConsole.printMessage("Contact added !!!");
                    PrintConsole.printDelimiter();
                }
            }
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteByValue(String value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Contact> findAll() {
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .headers(   "Authorization", "Bearer " + authService.getToken().getValue(),
                            "Accept", "application/json",
                            "Content-Type", "application/json")
                .uri(URI.create(properties.getProperty("mag.api.base.uri") + "contacts"))
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                ContactsResponse contactsResponse = objectMapper.readValue(httpResponse.body(), ContactsResponse.class);
                if (contactsResponse.isSuccess()) {
                    return contactsResponse.getContacts();
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        return  findAll().stream()
                .filter(contact -> contact.getContactType() == type)
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return  findAll().stream()
                .filter(contact -> contact.getValue().equals(value))
                .findFirst();
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString("{\"value\" : \"" + valueStart + "\"}"))
                .headers(   "Authorization", "Bearer " + authService.getToken().getValue(),
                            "Accept", "application/json",
                            "Content-Type", "application/json")
                .uri(URI.create(properties.getProperty("mag.api.base.uri") + "contacts/find"))
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                ContactsResponse contactsResponse = objectMapper.readValue(httpResponse.body(), ContactsResponse.class);
                if (contactsResponse.isSuccess()) {
                    return contactsResponse.getContacts();
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    @Override
    public List<Contact> findByName(String namePart) {
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .POST(HttpRequest.BodyPublishers.ofString("{\"name\" : \"" + namePart + "\"}"))
                .headers(   "Authorization", "Bearer " + authService.getToken().getValue(),
                            "Accept", "application/json",
                            "Content-Type", "application/json")
                .uri(URI.create(properties.getProperty("mag.api.base.uri") + "contacts/find"))
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                ContactsResponse contactsResponse = objectMapper.readValue(httpResponse.body(), ContactsResponse.class);
                if (contactsResponse.isSuccess()) {
                    return contactsResponse.getContacts();
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }
}