package ua.ithillel.dnipro.kremena.contactdirectory.service.auth.impl;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Token;
import ua.ithillel.dnipro.kremena.contactdirectory.model.User;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;

@RequiredArgsConstructor
public class ApiAuthService implements AuthService {

    private final AuthRepository authRepository;

    @Override
    public void login(User user) {
        authRepository.login(user);
    }

    @Override
    public void logout() {
        authRepository.logout();
    }

    @Override
    public void registration(User user) {
        authRepository.registration(user);
    }

    @Override
    public boolean isAuth() {
        return authRepository.isAuth();
    }

    @Override
    public Token getToken() {
        return authRepository.getToken();
    }

    @Override
    public User getAuthUser() {
        return authRepository.getAuthUser();
    }
}