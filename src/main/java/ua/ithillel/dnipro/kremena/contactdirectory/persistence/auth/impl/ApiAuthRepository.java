package ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.dto.LoginResponse;
import ua.ithillel.dnipro.kremena.contactdirectory.dto.RegisterResponse;
import ua.ithillel.dnipro.kremena.contactdirectory.dto.UsersResponse;
import ua.ithillel.dnipro.kremena.contactdirectory.dto.enums.ResponseStatus;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Token;
import ua.ithillel.dnipro.kremena.contactdirectory.model.User;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.util.console.PrintConsole;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Objects;
import java.util.Properties;

@Data
@RequiredArgsConstructor
public class ApiAuthRepository implements AuthRepository {

    private final HttpClient httpClient;
    private final Properties properties;
    private final ObjectMapper objectMapper;

    @Getter
    private Token token;

    @Getter
    private User authUser;

    @Override
    public void login(User user) {
        try {
            HttpRequest httpRequest =
                    HttpRequest.newBuilder()
                            .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(user)))
                            .uri(URI.create(properties.getProperty("mag.api.base.uri") + "login"))
                            .headers(   "Accept", "application/json",
                                        "Content-Type", "application/json")
                            .build();
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                LoginResponse loginResponse = objectMapper.readValue(httpResponse.body(), LoginResponse.class);
                if (loginResponse.isSuccess()) {
                    authUser = user;
                    token = new Token()
                            .setValue(loginResponse.getToken());
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void logout() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void registration(User user) {
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(objectMapper.writeValueAsString(user)))
                    .uri(URI.create(properties.getProperty("mag.api.base.uri") + "register"))
                    .headers(   "Accept", "application/json",
                                "Content-Type", "application/json")
                    .build();
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                RegisterResponse registerResponse = objectMapper.readValue(httpResponse.body(), RegisterResponse.class);
                if (registerResponse.isSuccess()) {
                    PrintConsole.printDelimiter();
                    PrintConsole.printMessage("User registered !!!");
                    PrintConsole.printDelimiter();
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isAuth() {
        if (Objects.isNull(token)) return false;
        else return isValidToken(token);
    }

    private boolean isValidToken(Token token) {
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(properties.getProperty("mag.api.base.uri") + "users2"))
                .headers(   "Authorization", "Bearer " + token.getValue(),
                            "Accept", "application/json",
                            "Content-Type", "application/json")
                .build();
        try {
            HttpResponse<String> httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (httpResponse.statusCode() == 200) {
                UsersResponse usersResponse = objectMapper.readValue(httpResponse.body(), UsersResponse.class);
                if(usersResponse.getResponseStatus() == ResponseStatus.OK) {
                    return true;
                }
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }
}