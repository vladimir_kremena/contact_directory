package ua.ithillel.dnipro.kremena.contactdirectory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.zaxxer.hikari.HikariDataSource;
import ua.ithillel.dnipro.kremena.contactdirectory.database.config.DatabaseConfig;
import ua.ithillel.dnipro.kremena.contactdirectory.database.template.JdbcTemplate;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.AuthRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.ContactRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.auth.biuld.BuilderAuthRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.contact.build.BuilderContactRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.service.contact.ContactService;
import ua.ithillel.dnipro.kremena.contactdirectory.service.factory.AuthServiceFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.service.factory.impl.auth.build.BuilderAuthServiceFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.Menu;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.factory.BuilderMenuItems;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.AuthView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ContactView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ErrorView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.impl.ConsoleAuthView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.impl.ConsoleContactView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.impl.ConsoleErrorView;
import ua.ithillel.dnipro.kremena.contactdirectory.util.encoder.PasswordEncoder;
import ua.ithillel.dnipro.kremena.contactdirectory.util.encoder.impl.MD5PasswordEncoder;
import ua.ithillel.dnipro.kremena.contactdirectory.util.property.ApplicationProperties;
import ua.ithillel.dnipro.kremena.contactdirectory.util.validator.Validator;
import ua.ithillel.dnipro.kremena.contactdirectory.util.validator.contact.ContactValidator;
import ua.ithillel.dnipro.kremena.contactdirectory.util.validator.contact.EmailValidator;
import ua.ithillel.dnipro.kremena.contactdirectory.util.validator.contact.PhoneValidator;

import javax.sql.DataSource;
import java.net.http.HttpClient;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class ApplicationBuilder {

    public static final String APPLICATION_MODE_KEY = "application.mode";

    public static void main(String[] args) {
        /* Properties */
        Properties properties = ApplicationProperties.getProperties(APPLICATION_MODE_KEY);
        /* http */
        HttpClient httpClient = HttpClient.newBuilder().build();
        /* json */
        ObjectMapper objectMapper = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        Validator<Contact> contactValidator = new ContactValidator(
                List.of(
                        new PhoneValidator(),
                        new EmailValidator()
                )
        );
        /* Database */
        DatabaseConfig databaseConfig = new DatabaseConfig(properties);
        DataSource dataSource = new HikariDataSource(databaseConfig.getHikariConfig());
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PasswordEncoder passwordEncoder = new MD5PasswordEncoder();
        /* Auth */
        BuilderAuthRepositoryFactory builderAuthRepositoryFactory =
                new BuilderAuthRepositoryFactory(httpClient, objectMapper, passwordEncoder, jdbcTemplate);
        AuthRepositoryFactory authRepositoryFactory =
                builderAuthRepositoryFactory.getAuthRepositoryFactory(properties, APPLICATION_MODE_KEY);
        AuthRepository authRepository = authRepositoryFactory.createAuthRepository();
        BuilderAuthServiceFactory builderAuthServiceFactory = new BuilderAuthServiceFactory(authRepository, 
                passwordEncoder);
        AuthServiceFactory authServiceFactory = builderAuthServiceFactory.getAuthServiceFactory(properties,
                APPLICATION_MODE_KEY);
        AuthService authService = authServiceFactory.createAuthService();
        /* Data */
        BuilderContactRepositoryFactory builderContactRepositoryFactory =
                new BuilderContactRepositoryFactory(httpClient, objectMapper, authService, jdbcTemplate);
        ContactRepositoryFactory repositoryFactory =
                builderContactRepositoryFactory.getContactRepositoryFactory(properties, APPLICATION_MODE_KEY);
        ContactRepository contactRepository = repositoryFactory.createContactRepository();
        ContactService contactService = new ContactService(contactRepository, contactValidator);
        /* Menu */
        Scanner scanner = new Scanner(System.in);
        ErrorView errorView = new ConsoleErrorView();
        ContactView contactView = new ConsoleContactView(scanner, errorView);
        AuthView authView = new ConsoleAuthView(scanner);
        BuilderMenuItems builderMenuItems =
                new BuilderMenuItems(contactView, errorView, authView, authService, contactService);
        Menu menu = new Menu(scanner, errorView, authService, builderMenuItems);
        menu.run();
    }
}