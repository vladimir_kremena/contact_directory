package ua.ithillel.dnipro.kremena.contactdirectory.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ua.ithillel.dnipro.kremena.contactdirectory.dto.enums.ResponseStatus;

@Data
public abstract class Response {
    @JsonProperty("status")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private ResponseStatus responseStatus;

    public boolean isSuccess() {
        return responseStatus == ResponseStatus.OK;
    }
}