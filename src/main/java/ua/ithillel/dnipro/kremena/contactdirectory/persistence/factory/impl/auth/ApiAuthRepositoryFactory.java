package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.impl.ApiAuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.AuthRepositoryFactory;

import java.net.http.HttpClient;
import java.util.Properties;

@RequiredArgsConstructor
public class ApiAuthRepositoryFactory implements AuthRepositoryFactory {

    private final HttpClient httpClient;
    private final Properties properties;
    private final ObjectMapper objectMapper;

    @Override
    public AuthRepository createAuthRepository() {
        return new ApiAuthRepository(httpClient, properties, objectMapper);
    }
}