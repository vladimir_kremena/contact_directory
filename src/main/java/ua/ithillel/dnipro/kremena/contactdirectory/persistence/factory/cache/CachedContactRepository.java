package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.cache;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
public class CachedContactRepository implements ContactRepository {

    private final ContactRepository contactRepository;
    private List<Contact> cacheAll;
    private Map<ContactType, List<Contact>> cacheType = new HashMap<>();

    @Override
    public void save(Contact contact) {
        contactRepository.save(contact);
        invalidateCache();
    }

    @Override
    public void deleteByValue(String value) {
        contactRepository.deleteByValue(value);
        invalidateCache();
    }

    @Override
    public List<Contact> findAll() {
        if (cacheAll == null) {
            cacheAll = contactRepository.findAll();
        }
        return cacheAll;
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        if (!cacheType.containsKey(type)) {
            cacheType.put(type, contactRepository.findByType(type));
        }
        return cacheType.get(type);
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return contactRepository.findByValue(value);
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return contactRepository.findByValueStart(valueStart);
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return contactRepository.findByName(namePart);
    }

    private void invalidateCache() {
        cacheAll = null;
        cacheType.clear();
    }

}