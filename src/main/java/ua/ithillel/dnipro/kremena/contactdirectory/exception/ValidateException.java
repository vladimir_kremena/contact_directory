package ua.ithillel.dnipro.kremena.contactdirectory.exception;

public class ValidateException extends RuntimeException {

    public ValidateException(String message) {
        super(message);
    }
}