package ua.ithillel.dnipro.kremena.contactdirectory.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@NoArgsConstructor
@Getter @Setter
@Accessors(chain = true)
public class Token {
    private String value;
    private LocalDateTime createTime;
    private LocalDateTime shutdownTime;
    private Boolean valid;

    public Boolean isValid() {
        return valid;
    }

    @Override
    public String toString() {
        return "Token [" +
                "value = " + value + " | " +
                "createTime = " + createTime + " | " +
                "shutdownTime = " + shutdownTime + " | " +
                "valid = " + valid +
                "]";
    }
}