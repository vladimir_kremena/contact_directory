package ua.ithillel.dnipro.kremena.contactdirectory.exception;

public class DuplicateContactException extends RuntimeException {

    public DuplicateContactException(String message) {
        super(message);
    }
}