package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.item;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.service.contact.ContactService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.MenuItem;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ContactView;

@RequiredArgsConstructor
public class ShowOnlyEmailMenuItem implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Show only emails";
    }

    @Override
    public void run() {
        contactView.showContacts(contactService.findEmails());
        contactView.pause();
    }
}