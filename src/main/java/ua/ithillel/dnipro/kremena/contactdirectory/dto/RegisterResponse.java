package ua.ithillel.dnipro.kremena.contactdirectory.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterResponse extends Response {
    private String error;
}