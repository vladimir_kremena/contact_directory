package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.item;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.MenuItem;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ErrorView;
import ua.ithillel.dnipro.kremena.contactdirectory.util.console.PrintConsole;

@RequiredArgsConstructor
public class LogoutMenuItem implements MenuItem {

    private final AuthService authService;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "LogOut";
    }

    @Override
    public void run() {
        try {
            authService.logout();
            PrintConsole.printMessage("Logout... ");
        } catch (UnsupportedOperationException uoE) {
            errorView.showError("Unsupported Operation");
        }
    }
}