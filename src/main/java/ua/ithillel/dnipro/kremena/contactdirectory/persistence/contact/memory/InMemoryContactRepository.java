package ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.memory;

import lombok.AllArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@AllArgsConstructor
public class InMemoryContactRepository implements ContactRepository {

    private List<Contact> contactList;

    @Override
    public void save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        contactList.add(contact);
    }

    @Override
    public void deleteByValue(String value) {
        contactList = contactList.stream()
                .filter(contact -> !contact.getValue().equals(value))
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public List<Contact> findAll() {
        return  contactList.stream()
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        return  contactList.stream()
                .filter(contact -> contact.getContactType() == type)
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return  contactList.stream()
                .filter(contact -> contact.getValue().equals(value))
                .findFirst();
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        if (valueStart.length() == 0) return new ArrayList<>();
        return  contactList.stream()
                .filter(contact -> contact.getValue().startsWith(valueStart))
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByName(String namePart) {
        if (namePart.length() == 0) return new ArrayList<>();
        return  contactList.stream()
                .filter(contact -> contact.getFullName().toLowerCase().contains(namePart.toLowerCase()))
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }
}