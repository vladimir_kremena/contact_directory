package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.item;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.service.contact.ContactService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.MenuItem;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ContactView;

@RequiredArgsConstructor
public class SearchByNameMenuItem implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;

    @Override
    public String getName() {
        return "Show matches by name";
    }

    @Override
    public void run() {
        contactView.showContacts(contactService.findByName(contactView.readNamePart()));
        contactView.pause();
    }
}