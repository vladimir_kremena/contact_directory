package ua.ithillel.dnipro.kremena.contactdirectory.util.codestyle;

public class SnakeCase {
    public static String toSnakeCase(String camelCase) {
        StringBuilder snakeCase = new StringBuilder();
        for (int i = 0; i < camelCase.length(); i++) {
            if (Character.isUpperCase(camelCase.charAt(i))) {
                if (i != 0) {
                    snakeCase.append('_');
                }
                snakeCase.append(Character.toLowerCase(camelCase.charAt(i)));
            } else {
                snakeCase.append(camelCase.charAt(i));
            }
        }
        return snakeCase.toString();
    }
}