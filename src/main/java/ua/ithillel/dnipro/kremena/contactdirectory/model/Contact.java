package ua.ithillel.dnipro.kremena.contactdirectory.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

@Data
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Contact implements Serializable {

    private String id;
    @JsonProperty("type")
    private ContactType contactType;
    @JsonIgnore
    private String firstName;
    @JsonIgnore
    private String lastName;
    private String value;

    @JsonProperty("name")
    public String getFullName() {
        return lastName + " " + firstName;
    }

    @JsonProperty("name")
    public void setFullName(String name) {
        String[] names = name.split(" ");
        if (names.length > 0) {
            if (Objects.nonNull(names[0])) lastName = names[0];
            if (Objects.nonNull(names[1])) firstName = names[1];
        }
    }

    public static Comparator<Contact> byLastNameComparator() {
        return (Contact current, Contact incoming) -> {
            if (current == null && incoming == null) return 0;
            if (current == null) return -1;
            if (incoming == null) return 1;
            int cmp = Objects.compare(current.lastName, incoming.lastName, String::compareTo);
            if (cmp == 0) {
                cmp = Objects.compare(current.firstName, incoming.firstName, String::compareTo);
            }
            return cmp;
        };
    }

    @Override
    public String toString() {
        return  id + " | " +
                contactType + " | " +
                firstName + " | " +
                lastName + " | " +
                value;
    }
}