package ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file.AbstractFileContact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class JsonFileContact extends AbstractFileContact {

    private final String fileName;
    private final ObjectMapper objectMapper;

    @Override
    public List<Contact> readAll() {
        isExists(fileName);
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            return objectMapper.readValue(bufferedReader, new TypeReference<ArrayList<Contact>>() {});
        } catch (IOException iOE) {
            return new ArrayList<>();
        }
    }

    @Override
    public void saveAll(List<Contact> contactList) {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(objectMapper.writeValueAsString(contactList));
            writer.flush();
        } catch (IOException iOE) {
            iOE.printStackTrace();
        }
    }
}