package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.contact;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.api.ApiContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.ContactRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;

import java.net.http.HttpClient;
import java.util.Properties;

@RequiredArgsConstructor
public class ApiContactRepositoryFactory implements ContactRepositoryFactory {

    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;
    private final AuthService authService;
    private final Properties properties;

    @Override
    public ContactRepository createContactRepository() {
        return new ApiContactRepository(httpClient, objectMapper, authService, properties);
    }

}