package ua.ithillel.dnipro.kremena.contactdirectory.ui.view;

import ua.ithillel.dnipro.kremena.contactdirectory.model.User;

public interface AuthView {

    User readUser();

    User createUser();

}