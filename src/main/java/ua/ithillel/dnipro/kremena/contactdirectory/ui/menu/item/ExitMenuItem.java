package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.item;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.MenuItem;
import ua.ithillel.dnipro.kremena.contactdirectory.util.console.PrintConsole;

@RequiredArgsConstructor
public class ExitMenuItem implements MenuItem {

    private final AuthService authService;

    @Override
    public String getName() {
        return "Exit";
    }

    @Override
    public void run() {
        PrintConsole.printMessage("Good by...");
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}