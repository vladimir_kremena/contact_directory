package ua.ithillel.dnipro.kremena.contactdirectory.util.encoder;

public interface PasswordEncoder {

    String encode(String password);

    boolean verify(String password, String hash);

}