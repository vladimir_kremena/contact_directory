package ua.ithillel.dnipro.kremena.contactdirectory.util.validator;

public interface Validator<T> {

    boolean isValid(T t);

}