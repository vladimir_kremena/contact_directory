package ua.ithillel.dnipro.kremena.contactdirectory.util.validator.contact;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.util.validator.Validator;

import java.util.List;

@RequiredArgsConstructor
public class ContactValidator implements Validator<Contact> {

    private final List<Validator<Contact>> validators;

    @Override
    public boolean isValid(Contact contact) {
        return validators.stream().allMatch(validator -> validator.isValid(contact));
    }
}