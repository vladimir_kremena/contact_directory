package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.auth;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.database.template.JdbcTemplate;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.impl.DatabaseAuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.AuthRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.util.encoder.PasswordEncoder;

@RequiredArgsConstructor
public class DatabaseAuthRepositoryFactory implements AuthRepositoryFactory {

    private final JdbcTemplate jdbcTemplate;
    private final PasswordEncoder passwordEncoder;

    @Override
    public AuthRepository createAuthRepository() {
        return new DatabaseAuthRepository(jdbcTemplate, passwordEncoder);
    }
}