package ua.ithillel.dnipro.kremena.contactdirectory.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ContactType {

    PHONE("Phone number"), EMAIL("E-mail");

    @Getter
    private final String displayName;

    @JsonCreator
    public static ContactType forValue(String value) {
        for (ContactType contactType : ContactType.values()) {
            if (contactType.toValue().equalsIgnoreCase(value)) {
                return contactType;
            }
        }
        return null;
    }

    @JsonValue
    public String toValue() {
        return name();
    }
}