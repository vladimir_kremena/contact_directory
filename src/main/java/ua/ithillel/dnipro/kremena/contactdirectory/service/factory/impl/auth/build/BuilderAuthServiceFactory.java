package ua.ithillel.dnipro.kremena.contactdirectory.service.factory.impl.auth.build;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.service.factory.AuthServiceFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.service.factory.impl.auth.ApiAuthServiceFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.service.factory.impl.auth.DatabaseAuthServiceFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.service.factory.impl.auth.InMemoryAuthServiceFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.util.encoder.PasswordEncoder;

import java.util.Map;
import java.util.Properties;

@RequiredArgsConstructor
public class BuilderAuthServiceFactory {

    private final AuthRepository authRepository;
    private final PasswordEncoder passwordEncoder;

    public AuthServiceFactory getAuthServiceFactory(Properties properties, String applicationMode) {
        String mode = properties.getProperty(applicationMode);
        Map<String, AuthServiceFactory> factories =
                Map.of(
                        "IN_MEMORY", new InMemoryAuthServiceFactory(authRepository),
                        "BYTE_FILE", new InMemoryAuthServiceFactory(authRepository),
                        "TEXT_FILE", new InMemoryAuthServiceFactory(authRepository),
                        "JSON_FILE", new InMemoryAuthServiceFactory(authRepository),
                        "API", new ApiAuthServiceFactory(authRepository),
                        "DB", new DatabaseAuthServiceFactory(authRepository, passwordEncoder)
                );
        if (!factories.containsKey(mode)) {
            throw new RuntimeException("Invalid mode configuration");
        }
        return factories.get(mode);
    }

}