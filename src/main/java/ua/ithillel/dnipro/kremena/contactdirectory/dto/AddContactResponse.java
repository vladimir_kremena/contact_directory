package ua.ithillel.dnipro.kremena.contactdirectory.dto;

import lombok.Data;

@Data
public class AddContactResponse extends Response {
    private String error;
}