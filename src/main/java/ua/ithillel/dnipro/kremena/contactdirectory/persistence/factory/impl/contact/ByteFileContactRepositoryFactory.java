package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.contact;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file.FileContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file.impl.ByteFileContact;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.ContactRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.cache.CachedContactRepository;

import java.util.Properties;

@RequiredArgsConstructor
public class ByteFileContactRepositoryFactory implements ContactRepositoryFactory {

    private final Properties properties;

    @Override
    public ContactRepository createContactRepository() {
        return new CachedContactRepository(
                new FileContactRepository(
                        new ByteFileContact(properties.getProperty("byte.file.name"))
                )
        );
    }
}