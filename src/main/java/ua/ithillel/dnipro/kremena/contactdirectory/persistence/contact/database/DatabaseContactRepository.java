package ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.database;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.database.mapper.ContactRowMapper;
import ua.ithillel.dnipro.kremena.contactdirectory.database.template.JdbcTemplate;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DatabaseContactRepository implements ContactRepository {

    private final JdbcTemplate jdbcTemplate;
    private final AuthService authService;

    @Override
    public void save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        jdbcTemplate.query(
                "insert into contacts(id, contact_type, first_name, last_name, value, user_id) " +
                        "values (?, ?, ?, ?, ?, ?)",
                new Object[] {
                        contact.getId(),
                        contact.getContactType().name(),
                        contact.getFirstName(),
                        contact.getLastName(),
                        contact.getValue(),
                        authService.getAuthUser().getId()
                }
        );
    }

    @Override
    public void deleteByValue(String value) {
        jdbcTemplate.query(
                "delete from contacts where value = ? and user_id = ?",
                new Object[] {
                        value,
                        authService.getAuthUser().getId()
                });
    }

    @Override
    public List<Contact> findAll() {
        return  jdbcTemplate.query(
                "select id, contact_type, first_name, last_name, value from contacts where user_id = ?",
                new Object[] {
                        authService.getAuthUser().getId()
                },
                new ContactRowMapper()
        ).stream()
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByType(ContactType type) {
        return  jdbcTemplate.query(
                "select id, contact_type, first_name, last_name, value from contacts where contact_type = ? and user_id = ?",
                new Object[] {
                        type.name(),
                        authService.getAuthUser().getId()
                },
                new ContactRowMapper()
        ).stream()
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return  jdbcTemplate.query(
                "select id, contact_type, first_name, last_name, value from contacts where value = ? and user_id = ?",
                new Object[] {
                        value,
                        authService.getAuthUser().getId()
                },
                new ContactRowMapper()
        ).stream()
                .findFirst();
    }

    @Override
    public List<Contact> findByValueStart(String valueStart) {
        return  jdbcTemplate.query(
                "select id, contact_type, first_name, last_name, value from contacts " +
                        "where value like concat(?, '%') and user_id = ?",
                new Object[] {
                        valueStart,
                        authService.getAuthUser().getId()
                },
                new ContactRowMapper()
        ).stream()
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }

    @Override
    public List<Contact> findByName(String namePart) {
        return  jdbcTemplate.query(
                "select id, contact_type, first_name, last_name, value from contacts " +
                        "where (last_name ilike concat('%', ?, '%') or first_name ilike concat('%', ?, '%')) and user_id = ?",
                new Object[] {
                        namePart,
                        namePart,
                        authService.getAuthUser().getId()
                },
                new ContactRowMapper()
        ).stream()
                .sorted(Contact.byLastNameComparator())
                .collect(Collectors.toList());
    }
}