package ua.ithillel.dnipro.kremena.contactdirectory.database.mapper;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.util.codestyle.SnakeCase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.function.Function;

@RequiredArgsConstructor
public class BeanPropertyRowMapper<T> implements Function<ResultSet, T> {

    private final Class<T> clazz;

    @Override
    public T apply(ResultSet resultSet) {
        Field[] fields = clazz.getDeclaredFields();
        try {
            T instance = clazz.getConstructor().newInstance();
            for (Field field : fields) {
                String name = SnakeCase.toSnakeCase(field.getName());
                Object value = resultSet.getObject(name, field.getType());
                field.setAccessible(true);
                field.set(instance, value);
            }
            return instance;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}