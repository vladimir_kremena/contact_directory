package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory;

import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;

public interface AuthRepositoryFactory {

    AuthRepository createAuthRepository();

}