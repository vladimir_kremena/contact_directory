package ua.ithillel.dnipro.kremena.contactdirectory.database.mapper;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.model.ContactType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

@RequiredArgsConstructor
public class ContactRowMapper implements Function<ResultSet, Contact> {

    @SneakyThrows
    @Override
    public Contact apply(ResultSet resultSet) {
        Contact contact = null;
        try {
            if (resultSet.getString("contact_type").equals(ContactType.PHONE.name())) {
                contact = new Contact()
                        .setId(resultSet.getString("id"))
                        .setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setContactType(ContactType.PHONE)
                        .setValue(resultSet.getString("value"));
            } else {
                contact = new Contact()
                        .setId(resultSet.getString("id"))
                        .setFirstName(resultSet.getString("first_name"))
                        .setLastName(resultSet.getString("last_name"))
                        .setContactType(ContactType.EMAIL)
                        .setValue(resultSet.getString("value"));
            }
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
        return contact;
    }
}