package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.contact;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.database.template.JdbcTemplate;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.ContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.database.DatabaseContactRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.ContactRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;

@RequiredArgsConstructor
public class DatabaseContactRepositoryFactory implements ContactRepositoryFactory {

    private final JdbcTemplate jdbcTemplate;
    private final AuthService authService;

    @Override
    public ContactRepository createContactRepository() {
        return new DatabaseContactRepository(jdbcTemplate, authService);
    }


}