package ua.ithillel.dnipro.kremena.contactdirectory.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse extends Response {
    private String token;
    private String error;
}