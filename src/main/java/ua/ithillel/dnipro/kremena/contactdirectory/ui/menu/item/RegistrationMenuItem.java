package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.item;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.MenuItem;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.AuthView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ErrorView;

@RequiredArgsConstructor
public class RegistrationMenuItem implements MenuItem {

    private final AuthService authService;
    private final AuthView authView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Registration";
    }

    @Override
    public void run() {
        try {
            authService.registration(authView.createUser());
        } catch (UnsupportedOperationException uOE) {
            errorView.showError("Operation not supported!!!");
        }
    }
}