package ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file.impl;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.contact.file.AbstractFileContact;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class ByteFileContact extends AbstractFileContact {

    private final String fileName;

    @Override
    public List<Contact> readAll() {
        isExists(fileName);
        List<Contact> contactList = null;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName))) {
            contactList = (List<Contact>) objectInputStream.readObject();
        } catch (EOFException eOFE) {
            return new ArrayList<>();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return contactList;
    }

    @Override
    public void saveAll(List<Contact> contactList) {
        try(ObjectOutputStream objectOutputStream =
                    new ObjectOutputStream(new FileOutputStream(fileName))) {
            objectOutputStream.writeObject(contactList);
            objectOutputStream.flush();
        } catch (IOException iOE) {
            iOE.printStackTrace();
        }
    }
}