package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.contact.build;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.database.template.JdbcTemplate;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.ContactRepositoryFactory;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.contact.*;
import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;

import java.net.http.HttpClient;
import java.util.Map;
import java.util.Properties;

@RequiredArgsConstructor
public class BuilderContactRepositoryFactory {

    private final HttpClient httpClient;
    private final ObjectMapper objectMapper;
    private final AuthService authService;
    private final JdbcTemplate jdbcTemplate;

    public ContactRepositoryFactory getContactRepositoryFactory(Properties properties, String applicationMode) {
        String mode = properties.getProperty(applicationMode);
        Map<String, ContactRepositoryFactory> factories =
                Map.of(
                        "IN_MEMORY", new InMemoryFileContactRepositoryFactory(),
                        "BYTE_FILE", new ByteFileContactRepositoryFactory(properties),
                        "TEXT_FILE", new TextFileContactRepositoryFactory(properties),
                        "JSON_FILE", new JsonFileContactRepositoryFactory(properties),
                        "API", new ApiContactRepositoryFactory(httpClient, objectMapper, authService, properties),
                        "DB", new DatabaseContactRepositoryFactory(jdbcTemplate, authService)
                );
        if (!factories.containsKey(mode)) {
            throw new RuntimeException("Invalid mode configuration");
        }
        return factories.get(mode);
    }
}