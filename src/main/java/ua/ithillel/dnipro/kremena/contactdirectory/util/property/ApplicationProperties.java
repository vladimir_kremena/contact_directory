package ua.ithillel.dnipro.kremena.contactdirectory.util.property;

import ua.ithillel.dnipro.kremena.contactdirectory.ApplicationBuilder;

import java.io.FileReader;
import java.util.Optional;
import java.util.Properties;

public class ApplicationProperties {
    public static Properties getProperties(String applicationMode) {
        String propertiesSource = System.getProperty("property.source");
        propertiesSource = Optional.ofNullable(propertiesSource).orElse("CLASSPATH");
        Properties properties = new Properties();
        try {
            if (propertiesSource.equals("CLASSPATH")) {
                properties.load(
                        ApplicationBuilder.class
                                .getClassLoader()
                                .getResourceAsStream("default_config.properties"));
            } else {
                properties.load(new FileReader(propertiesSource));
                properties.load(new FileReader("config/database_config.properties"));
            }
        } catch (Exception e) {
            properties.setProperty(applicationMode, "IN_MEMORY");
        }
        return properties;
    }
}