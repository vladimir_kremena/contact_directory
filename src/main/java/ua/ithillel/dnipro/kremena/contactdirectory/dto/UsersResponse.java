package ua.ithillel.dnipro.kremena.contactdirectory.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import ua.ithillel.dnipro.kremena.contactdirectory.model.User;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UsersResponse extends Response {

    private List<User> users;
    private String error;

    public UsersResponse() {
    }

    public UsersResponse(List<User> users, String error) {
        this.users = users;
        this.error = error;
    }
}