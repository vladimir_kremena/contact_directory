package ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.impl.auth;

import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.impl.InMemoryAuthRepository;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.factory.AuthRepositoryFactory;

public class InMemoryAuthRepositoryFactory implements AuthRepositoryFactory {

    @Override
    public AuthRepository createAuthRepository() {
        return new InMemoryAuthRepository();
    }
}