package ua.ithillel.dnipro.kremena.contactdirectory.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import ua.ithillel.dnipro.kremena.contactdirectory.model.Contact;

import java.util.List;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContactsResponse extends Response {
    private List<Contact> contacts;
}