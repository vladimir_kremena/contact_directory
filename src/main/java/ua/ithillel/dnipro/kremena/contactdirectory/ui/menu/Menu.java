package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu;

import ua.ithillel.dnipro.kremena.contactdirectory.service.auth.AuthService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.factory.BuilderMenuItems;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ErrorView;
import ua.ithillel.dnipro.kremena.contactdirectory.util.console.PrintConsole;

import java.util.List;
import java.util.Scanner;

public class Menu {

    private final Scanner scanner;
    private final ErrorView errorView;
    private final AuthService authService;
    private final BuilderMenuItems builderMenuItems;
    private List<MenuItem> items;

    public Menu(Scanner scanner, ErrorView errorView, AuthService authService, BuilderMenuItems builderMenuItems) {
        this.scanner = scanner;
        this.errorView = errorView;
        this.authService = authService;
        this.builderMenuItems = builderMenuItems;
    }

    public void run() {

        while(true) {
            items = builderMenuItems.getMenuItems(authService.isAuth());
            showMenu();
            int choice = getChoiceUser();
            if(choice >= 0 && choice < items.size()) {
                MenuItem item = items.get(choice);
                item.run();
                if (item.isFinal()) {
                    return;
                }
            } else {
                errorView.showError("Invalid choice, try again ... ");
            }
        }
    }

    private void showMenu() {
        PrintConsole.printDelimiter();
        PrintConsole.printMessage("Contact directory");
        PrintConsole.printMessage("Menu");
        PrintConsole.printMenu(items);
        PrintConsole.printInnerDelimiter();
    }

    private int getChoiceUser() {
        PrintConsole.printRequestMessage("Enter your choice");
        int choice = 0;
        try {
            choice = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException nFE) {}
        return choice - 1;
    }
}