package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu;

public interface MenuItem {

    String getName();

    void run();

    default boolean isFinal() {
        return false;
    }

}