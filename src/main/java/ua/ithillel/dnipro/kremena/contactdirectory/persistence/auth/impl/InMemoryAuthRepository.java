package ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.impl;

import ua.ithillel.dnipro.kremena.contactdirectory.model.Token;
import ua.ithillel.dnipro.kremena.contactdirectory.model.User;
import ua.ithillel.dnipro.kremena.contactdirectory.persistence.auth.AuthRepository;

public class InMemoryAuthRepository implements AuthRepository {

    private final static String LOGIN = "admin";
    private final static String PASSWD = "admin";
    private boolean auth = false;

    @Override
    public void login(User user) {
        if (!user.getLogin().equals(LOGIN) || !user.getPassword().equals(PASSWD)) {
            throw new IllegalArgumentException();
        }
        auth = true;
    }

    @Override
    public void logout() {
        auth = false;
    }

    @Override
    public void registration(User user) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isAuth() {
        return auth;
    }

    @Override
    public Token getToken() {
        throw new UnsupportedOperationException();
    }

    @Override
    public User getAuthUser() {
        return new User()
                .setLogin(LOGIN)
                .setPassword(PASSWD);
    }
}