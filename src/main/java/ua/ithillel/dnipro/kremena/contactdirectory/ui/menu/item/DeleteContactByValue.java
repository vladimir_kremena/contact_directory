package ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.item;

import lombok.RequiredArgsConstructor;
import ua.ithillel.dnipro.kremena.contactdirectory.exception.ContactNotFoundException;
import ua.ithillel.dnipro.kremena.contactdirectory.service.contact.ContactService;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.menu.MenuItem;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ContactView;
import ua.ithillel.dnipro.kremena.contactdirectory.ui.view.ErrorView;

@RequiredArgsConstructor
public class DeleteContactByValue implements MenuItem {

    private final ContactService contactService;
    private final ContactView contactView;
    private final ErrorView errorView;

    @Override
    public String getName() {
        return "Delete by value";
    }

    @Override
    public void run() {
        try {
            contactService.deleteByValue(contactView.readValue());
        } catch (ContactNotFoundException e) {
            errorView.showError(e.getMessage());
        } catch (UnsupportedOperationException e) {
            errorView.showError("Unsupported Operation");
        }
    }
}