create table users (
    id                  serial8 primary key,
    login               varchar(32) unique not null,
    password            varchar(32) not null,
    date_born           date not null,
    date_registration   timestamp not null default now()
);

create table contacts (
    id                  char(36) unique primary key,
    contact_type        char(5) not null,
    first_name          varchar(32) not null,
    last_name           varchar(32) not null,
    value               varchar(32) unique not null,
    user_id             serial8 not null references users(id)
);

create table tokens (
    value               char(36) primary key,
    create_time         timestamp not null default now(),
    shutdown_time       timestamp,
    valid               boolean not null,
    user_id             serial8 not null references users(id)
);